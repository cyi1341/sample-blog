# sample-blog
## About
This is a project I made for someone else for fun to kill time and make myself look like I'm useful

Currently the site is licensed under CC-BY-SA 4.0, though the licensing is subject to change for future updates
## Todo list
 - when scrolling, the left side stay still while the right side scrolls as usual (pc) (CSS)
 - bottom left would be sections for not home page (for example, in blogs, bottom left would be blog entries) (pc/mobile) (HTML+CSS)
 - make it so that the side/menu works like expandable folder links (like in NNN the file browser), with it being collapsed by default (or make it an option for the site owner) (CSS)
 - make it work with Hugo the static site generator
 - make it so that it works for custom sections 
 - options to make modifying/adding content easy
 - documentation

